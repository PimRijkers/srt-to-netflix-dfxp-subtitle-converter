﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRT_to_Netflix_DFXP_Subtitle_Converter
{
    class Converter
    {
        public string Convert(string input, int delayTime_ms, decimal subtitleFPS)
        {
            if (String.IsNullOrWhiteSpace(input)) { return ""; }

            // Create subtitle objects
            var subtitleObjects = CreateSubtitleObjects(input);
            if (subtitleObjects.Count == 0) { return ""; }

            // Scale subtitle times to 24 FPS if the subtitle file wasn't made for a 24 FPS video
            if (subtitleFPS != 24)
            {
                foreach (var subtitle in subtitleObjects)
                {
                    subtitle.ScaleTo24FPS(subtitleFPS);
                }
            }

            // Add the delay
            // Note that no check is done to ensure no negative time is set, this makes it possible to resync subtitles when the offset has changed during a movie
            if (delayTime_ms != 0)
            {
                var delayTimeSpan = TimeSpan.FromMilliseconds(delayTime_ms);
                foreach (var subtitle in subtitleObjects)
                {
                    subtitle.Delay(delayTimeSpan);
                }
            }

            // Switch the subtitle sign positions
            SwitchSignPositions(subtitleObjects);

            // Create DFXP subtitle string
            var DFXPSubtitleString = CreateDFXPSubtitlesString(subtitleObjects);
            return DFXPSubtitleString;
        }

        private List<Subtitle> CreateSubtitleObjects(string input)
        {
            // Split into lines
            var inputLines = input.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();

            // Get the line numbers of the split between subtitles
            var splitLineNumbers = new List<int>() { -1 }; // Start with -1 to include the first subtitle
            for (var lineNumber = 0; lineNumber < inputLines.Count; lineNumber++)
            {
                var line = inputLines[lineNumber];
                if (String.IsNullOrEmpty(line)) // Split at empty lines between two subtitles
                {
                    splitLineNumbers.Add(lineNumber);
                }
            }

            // Create the subtitle objects
            var subtitles = new List<Subtitle>();
            foreach (var splitLineNumber in splitLineNumbers)
            {
                var validSubtitle = Subtitle.Create(splitLineNumber, inputLines, out var subtitle);
                if (validSubtitle)
                {
                    subtitles.Add(subtitle);
                }
            }

            return subtitles;
        }

        private void SwitchSignPositions(List<Subtitle> subtitleObjects)
        {
            for (int i = 0; i < subtitleObjects.Count; i++)
            {
                var subtitle = subtitleObjects[i];
                for (int j = 0; j < subtitle.SubtitleLines.Count; j++)
                {
                    var subtitleLine = subtitle.SubtitleLines[j];

                    // Get the signs before the sentence
                    var preSentenceSignCount = 0;
                    while (preSentenceSignCount < subtitleLine.Length && IsSign(subtitleLine[preSentenceSignCount])) { preSentenceSignCount++; }
                    var preSentenceSigns = subtitleLine.Substring(0, preSentenceSignCount);

                    // Get the signs after the sentence
                    var postSentenceSignCount = 0;
                    while (postSentenceSignCount >= 0 && IsSign(subtitleLine[subtitleLine.Length - 1 - postSentenceSignCount])) { postSentenceSignCount++; }
                    var postSentenceSigns = subtitleLine.Substring(subtitleLine.Length - postSentenceSignCount, postSentenceSignCount);

                    // Get the new subtitle line with the switched signs
                    var subtitleLineWithoutSigns = subtitleLine.Substring(preSentenceSignCount, subtitleLine.Length - preSentenceSignCount - postSentenceSignCount);
                    var newSubtitleLine = new string(postSentenceSigns.Reverse().ToArray()) + subtitleLineWithoutSigns + new string(preSentenceSigns.Reverse().ToArray());

                    // Replace the subtitle line in the subtitles object with the new one
                    subtitle.SubtitleLines[j] = newSubtitleLine;
                }
            }
        }

        private bool IsSign(char character)
        {
            if ((character >= 32 && character <= 47) ||
                (character >= 58 && character <= 64) ||
                (character >= 91 && character <= 96) ||
                (character >= 123 && character <= 126))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        private string CreateDFXPSubtitlesString(List<Subtitle> subtitleObjects)
        {
            // Create the string builder and add the leading strings
            var subtitlesStringBuilder = new StringBuilder(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<tt xml:lang='en' xmlns='http://www.w3.org/2006/10/ttaf1' xmlns:tts='http://www.w3.org/2006/10/ttaf1#style'>\n" +
                "<head></head>\n" +
                "<body>\n" +
                "<div xml:id=\"captions\">");

            // Add a line for each subtitle
            foreach (var subtitle in subtitleObjects)
            {
                var subtitleString = "\n<p begin=\"" + subtitle.BeginTime.ToString(@"hh\:mm\:ss\.fff") + "\" end=\"" + subtitle.EndTime.ToString(@"hh\:mm\:ss\.fff") + "\">";

                // Combine the subtitle lines of a subtitle
                var isFirstSubtitleLine = true;
                foreach (var subtitleLine in subtitle.SubtitleLines)
                {
                    if (isFirstSubtitleLine == false) { subtitleString += "<br />"; }
                    else { isFirstSubtitleLine = false; }

                    subtitleString += subtitleLine;
                }

                subtitleString += "</p>";
                subtitlesStringBuilder.Append(subtitleString);
            }

            // Add the trailing strings to the string builder
            subtitlesStringBuilder.Append("\n</div>\n</body>\n</tt>");

            return subtitlesStringBuilder.ToString();
        }
    }

    internal class Subtitle
    {
        public TimeSpan BeginTime { get; private set; } = new TimeSpan();
        public TimeSpan EndTime { get; private set; } = new TimeSpan();
        public List<string> SubtitleLines { get; private set; } = new List<string>();

        public static bool Create(int splitLineNumber, List<string> inputLines, out Subtitle subtitle)
        {
            subtitle = new Subtitle();

            // Checks to make sure no problems occur at the end of the file
            if (splitLineNumber + 3 < inputLines.Count && // Prevent going out of bounds while finding the begin and end time
                Int32.TryParse(inputLines[splitLineNumber + 1], out var unusedResult)) // Check if the line after an empty line contains a number
            {
                // Get the begin and end time
                var timeLineNumber = splitLineNumber + 2;
                var timeLineSplit = inputLines[timeLineNumber].Split(' ');
                var beginTimeString = timeLineSplit[0];
                var endTimeString = timeLineSplit[2];

                // Try to parse the begin and end time
                subtitle.BeginTime = TimeSpan.ParseExact(beginTimeString, @"hh\:mm\:ss\,fff", CultureInfo.InvariantCulture);
                subtitle.EndTime = TimeSpan.ParseExact(endTimeString, @"hh\:mm\:ss\,fff", CultureInfo.InvariantCulture);

                // Get the subtitle lines
                int currentLineNumber = splitLineNumber + 3;
                while (currentLineNumber < inputLines.Count && // Prevent going out of bounds while finding the lines of this subtitle
                    String.IsNullOrWhiteSpace(inputLines[currentLineNumber]) == false) // Stop when the line is empty, because that's the end of the subtitle
                {
                    subtitle.SubtitleLines.Add(inputLines[currentLineNumber]);
                    currentLineNumber++;
                }

                // Return true if the subtitle is valid
                if (subtitle.SubtitleLines.Count > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public void ScaleTo24FPS(decimal subtitleFPS)
        {
            decimal scale = subtitleFPS / 24;

            BeginTime = TimeSpan.FromTicks((long)Math.Floor(BeginTime.Ticks * scale));
            EndTime = TimeSpan.FromTicks((long)Math.Floor(EndTime.Ticks * scale));
        }

        public void Delay(TimeSpan delayTime)
        {
            BeginTime += delayTime;
            EndTime += delayTime;
        }
    }
}
