﻿namespace SRT_to_Netflix_DFXP_Subtitle_Converter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labelLastConvert = new System.Windows.Forms.Label();
            this.buttonConvert = new System.Windows.Forms.Button();
            this.labelDelayMs = new System.Windows.Forms.Label();
            this.numericUpDownDelayTime = new System.Windows.Forms.NumericUpDown();
            this.labelDelay = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.labelSave = new System.Windows.Forms.Label();
            this.labelOpen = new System.Windows.Forms.Label();
            this.textBoxSave = new System.Windows.Forms.TextBox();
            this.textBoxOpen = new System.Windows.Forms.TextBox();
            this.labelSubtitleFps = new System.Windows.Forms.Label();
            this.comboBoxSubtitleFps = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDelayTime)).BeginInit();
            this.SuspendLayout();
            // 
            // labelLastConvert
            // 
            this.labelLastConvert.AutoSize = true;
            this.labelLastConvert.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelLastConvert.Location = new System.Drawing.Point(133, 126);
            this.labelLastConvert.Name = "labelLastConvert";
            this.labelLastConvert.Size = new System.Drawing.Size(133, 13);
            this.labelLastConvert.TabIndex = 13;
            this.labelLastConvert.Text = "Last convert was done at: ";
            // 
            // buttonConvert
            // 
            this.buttonConvert.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonConvert.Location = new System.Drawing.Point(52, 121);
            this.buttonConvert.Name = "buttonConvert";
            this.buttonConvert.Size = new System.Drawing.Size(75, 23);
            this.buttonConvert.TabIndex = 12;
            this.buttonConvert.Text = "Convert";
            this.buttonConvert.UseVisualStyleBackColor = true;
            this.buttonConvert.Click += new System.EventHandler(this.ButtonConvert_Click);
            // 
            // labelDelayMs
            // 
            this.labelDelayMs.AutoSize = true;
            this.labelDelayMs.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelDelayMs.Location = new System.Drawing.Point(134, 67);
            this.labelDelayMs.Name = "labelDelayMs";
            this.labelDelayMs.Size = new System.Drawing.Size(20, 13);
            this.labelDelayMs.TabIndex = 9;
            this.labelDelayMs.Text = "ms";
            // 
            // numericUpDownDelayTime
            // 
            this.numericUpDownDelayTime.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDownDelayTime.Location = new System.Drawing.Point(53, 64);
            this.numericUpDownDelayTime.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDownDelayTime.Minimum = new decimal(new int[] {
            10000000,
            0,
            0,
            -2147483648});
            this.numericUpDownDelayTime.Name = "numericUpDownDelayTime";
            this.numericUpDownDelayTime.Size = new System.Drawing.Size(74, 20);
            this.numericUpDownDelayTime.TabIndex = 8;
            this.numericUpDownDelayTime.ThousandsSeparator = true;
            // 
            // labelDelay
            // 
            this.labelDelay.AutoSize = true;
            this.labelDelay.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelDelay.Location = new System.Drawing.Point(11, 66);
            this.labelDelay.Name = "labelDelay";
            this.labelDelay.Size = new System.Drawing.Size(34, 13);
            this.labelDelay.TabIndex = 7;
            this.labelDelay.Text = "Delay";
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Image = ((System.Drawing.Image)(resources.GetObject("buttonSave.Image")));
            this.buttonSave.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonSave.Location = new System.Drawing.Point(501, 34);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(22, 22);
            this.buttonSave.TabIndex = 6;
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // buttonOpen
            // 
            this.buttonOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpen.Image = ((System.Drawing.Image)(resources.GetObject("buttonOpen.Image")));
            this.buttonOpen.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonOpen.Location = new System.Drawing.Point(501, 8);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(22, 22);
            this.buttonOpen.TabIndex = 3;
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.ButtonOpen_Click);
            // 
            // labelSave
            // 
            this.labelSave.AutoSize = true;
            this.labelSave.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelSave.Location = new System.Drawing.Point(12, 38);
            this.labelSave.Name = "labelSave";
            this.labelSave.Size = new System.Drawing.Size(32, 13);
            this.labelSave.TabIndex = 4;
            this.labelSave.Text = "Save";
            // 
            // labelOpen
            // 
            this.labelOpen.AutoSize = true;
            this.labelOpen.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelOpen.Location = new System.Drawing.Point(12, 12);
            this.labelOpen.Name = "labelOpen";
            this.labelOpen.Size = new System.Drawing.Size(33, 13);
            this.labelOpen.TabIndex = 1;
            this.labelOpen.Text = "Open";
            // 
            // textBoxSave
            // 
            this.textBoxSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSave.Location = new System.Drawing.Point(53, 35);
            this.textBoxSave.Name = "textBoxSave";
            this.textBoxSave.Size = new System.Drawing.Size(442, 20);
            this.textBoxSave.TabIndex = 5;
            // 
            // textBoxOpen
            // 
            this.textBoxOpen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOpen.Location = new System.Drawing.Point(53, 9);
            this.textBoxOpen.Name = "textBoxOpen";
            this.textBoxOpen.ReadOnly = true;
            this.textBoxOpen.Size = new System.Drawing.Size(442, 20);
            this.textBoxOpen.TabIndex = 2;
            // 
            // labelSubtitleFps
            // 
            this.labelSubtitleFps.AutoSize = true;
            this.labelSubtitleFps.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelSubtitleFps.Location = new System.Drawing.Point(133, 96);
            this.labelSubtitleFps.Name = "labelSubtitleFps";
            this.labelSubtitleFps.Size = new System.Drawing.Size(65, 13);
            this.labelSubtitleFps.TabIndex = 11;
            this.labelSubtitleFps.Text = "Subtitle FPS";
            // 
            // comboBoxSubtitleFps
            // 
            this.comboBoxSubtitleFps.FormattingEnabled = true;
            this.comboBoxSubtitleFps.Location = new System.Drawing.Point(53, 92);
            this.comboBoxSubtitleFps.Name = "comboBoxSubtitleFps";
            this.comboBoxSubtitleFps.Size = new System.Drawing.Size(74, 21);
            this.comboBoxSubtitleFps.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 153);
            this.Controls.Add(this.comboBoxSubtitleFps);
            this.Controls.Add(this.labelSubtitleFps);
            this.Controls.Add(this.labelLastConvert);
            this.Controls.Add(this.buttonConvert);
            this.Controls.Add(this.labelDelayMs);
            this.Controls.Add(this.numericUpDownDelayTime);
            this.Controls.Add(this.labelDelay);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonOpen);
            this.Controls.Add(this.labelSave);
            this.Controls.Add(this.labelOpen);
            this.Controls.Add(this.textBoxSave);
            this.Controls.Add(this.textBoxOpen);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(3840, 192);
            this.MinimumSize = new System.Drawing.Size(400, 192);
            this.Name = "Form1";
            this.Text = "SRT to Netflix DFXP Converter";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDelayTime)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelLastConvert;
        private System.Windows.Forms.Button buttonConvert;
        private System.Windows.Forms.Label labelDelayMs;
        private System.Windows.Forms.NumericUpDown numericUpDownDelayTime;
        private System.Windows.Forms.Label labelDelay;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.Label labelSave;
        private System.Windows.Forms.Label labelOpen;
        private System.Windows.Forms.TextBox textBoxSave;
        private System.Windows.Forms.TextBox textBoxOpen;
        private System.Windows.Forms.Label labelSubtitleFps;
        private System.Windows.Forms.ComboBox comboBoxSubtitleFps;
    }
}

