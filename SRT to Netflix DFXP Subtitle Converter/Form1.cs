﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SRT_to_Netflix_DFXP_Subtitle_Converter
{
    public partial class Form1 : Form
    {
        readonly List<decimal> SubtitleFpsList = new List<decimal>() { (decimal) 23.976, (decimal) 23.98, 24, 25 };

        public Form1()
        {
            InitializeComponent();

            SetDarkTheme(DarkThemeMode.System);

            comboBoxSubtitleFps.DataSource = SubtitleFpsList;
        }

        private void ButtonOpen_Click(object sender, EventArgs e)
        {
            // Open the dialog to select the SRT file
            var openDialog = new OpenFileDialog()
            {
                Title = "Select the SRT subtitle file",
                Filter = "SRT file (*.srt)|*.srt|All files (*.*)|*.*",
            };

            // Set initial directory of the dialog
            var initialDirectorySet = false;
            var openFileLocation = textBoxOpen.Text;
            var lastOpenBackslashIndex = openFileLocation.LastIndexOf('\\');
            if (lastOpenBackslashIndex != -1)
            {
                var lastOpenDirectory = openFileLocation.Remove(lastOpenBackslashIndex);
                if (Directory.Exists(lastOpenDirectory))
                {
                    openDialog.InitialDirectory = lastOpenDirectory;
                    initialDirectorySet = true;
                }
            }

            if (initialDirectorySet == false)
            {
                openDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            }


            // Open the dialog and fill the textbox with the selected file location
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxOpen.Text = openDialog.FileName;

                // Automatically set the save location if none was set
                if (String.IsNullOrWhiteSpace(textBoxSave.Text))
                {
                    var newOpenFileLocation = textBoxOpen.Text;
                    var lastDotIndex = newOpenFileLocation.LastIndexOf('.');
                    var saveFileLocation = newOpenFileLocation.Remove(lastDotIndex) + ".dfxp";

                    textBoxSave.Text = saveFileLocation;
                }
            }
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            var saveDialog = new OpenFileDialog()
            {
                Title = "Select the save file location",
                Filter = "DFXP file (*.dfxp)|*.dfxp|All files (*.*)|*.*",
            };

            // Set initial directory of the dialog
            var initialDirectorySet = false;

            // First attempt to reuse the last save location
            var lastSaveBackslashIndex = textBoxSave.Text.LastIndexOf('\\');
            if (lastSaveBackslashIndex != -1) // Check if the location contains a directory
            {
                var lastSaveDirectory = textBoxSave.Text.Remove(lastSaveBackslashIndex);
                if (Directory.Exists(lastSaveDirectory)) // Check if the location exists
                {
                    saveDialog.InitialDirectory = lastSaveDirectory;
                    initialDirectorySet = true;
                }
            }

            // Otherwise attempt to use the current open location
            if (initialDirectorySet == false)
            {
                var lastOpenBackslashIndex = textBoxOpen.Text.LastIndexOf('\\');
                if (lastOpenBackslashIndex != -1) // Check if the location contains a directory
                {
                    var currentOpenDirectory = textBoxOpen.Text.Remove(lastOpenBackslashIndex);
                    if (Directory.Exists(currentOpenDirectory)) // Check if the location exists
                    {
                        saveDialog.InitialDirectory = currentOpenDirectory;
                        initialDirectorySet = true;
                    }
                }
            }

            // Otherwise use the desktop location, which is used as the default
            if (initialDirectorySet == false)
            {
                saveDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            }


            // Set initial file name
            var dotIndex = textBoxOpen.Text.LastIndexOf('.');
            if (dotIndex != -1)
            {
                var openFileLocation = textBoxOpen.Text;
                var lastOpenBackslashIndex = openFileLocation.LastIndexOf('\\');
                var lastDotIndex = openFileLocation.LastIndexOf('.');
                var saveFileName = openFileLocation.Substring(lastOpenBackslashIndex + 1, lastDotIndex - lastOpenBackslashIndex - 1); // File name without extension
                saveDialog.FileName = saveFileName + ".dfxp";
            }


            // Open the dialog and fill the textbox with the selected file location
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxSave.Text = saveDialog.FileName;
            }
        }

        private async void ButtonConvert_Click(object sender, EventArgs e)
        {
            await ConvertWithAsyncIO();
        }

        private async Task ConvertWithAsyncIO()
        {
            // Get SRT file content
            var input = "";
            try
            {
                using (var fileStream = new FileStream(textBoxOpen.Text, FileMode.Open, FileAccess.Read))
                {
                    using (var streamReader = new StreamReader(fileStream))
                    {
                        input = await streamReader.ReadToEndAsync();
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Reading the SRT file failed." + Environment.NewLine + ex.Message);
                return;
            }


            // Convert the file content
            string output;
            try
            {
                var converter = new Converter();
                var delayTime = Int32.Parse(numericUpDownDelayTime.Value.ToString());
                var subtitleFps = Decimal.Parse(comboBoxSubtitleFps.Text);

                output = converter.Convert(input, delayTime, subtitleFps);
                labelLastConvert.Text = "Last convert was done at: " + DateTime.Now;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Converting the file failed." + Environment.NewLine + ex.Message);
                return;
            }


            // Write DFXP file content
            try
            {
                using (var fileStream = new FileStream(textBoxSave.Text, FileMode.Create, FileAccess.Write))
                {
                    using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
                    {
                        await streamWriter.WriteAsync(output);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Writing the DFXP file failed." + Environment.NewLine + ex.Message);
            }
        }

        void SetDarkTheme(DarkThemeMode mode)
        {
            var useDarkTheme = false;

            // Check if the dark theme should be used
            if (mode == DarkThemeMode.Never) { return; }
            else if (mode == DarkThemeMode.Always) { useDarkTheme = true; }
            else // mode == Mode.System
            {
                // Get the windows application theme
                var doAppsUseLightTheme = (int)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize", "AppsUseLightTheme", 1); // Returns 0 for dark theme, otherwise 1
                if (doAppsUseLightTheme == 0) { useDarkTheme = true; }
            }

            
            // Use the dark theme
            if (useDarkTheme)
            {
                var darkGrey = Color.FromArgb(32, 32, 32);
                var mediumGrey = Color.FromArgb(50, 50, 50);
                var mediumGreyDisabled = Color.FromArgb(77, 77, 77);
                var lightGrey = Color.FromArgb(100, 100, 100);
                var white = Color.FromName("white");

                this.ForeColor = white;
                this.BackColor = darkGrey;

                textBoxOpen.ForeColor = white;
                textBoxOpen.BackColor = mediumGreyDisabled;
                textBoxOpen.BorderStyle = BorderStyle.FixedSingle;
                buttonOpen.ForeColor = lightGrey;
                buttonOpen.BackColor = mediumGrey;
                buttonOpen.FlatStyle = FlatStyle.Flat;
                buttonOpen.Padding = new Padding(0, 0, 2, 2);

                textBoxSave.ForeColor = white;
                textBoxSave.BackColor = mediumGrey;
                textBoxSave.BorderStyle = BorderStyle.FixedSingle;
                buttonSave.ForeColor = lightGrey;
                buttonSave.BackColor = mediumGrey;
                buttonSave.FlatStyle = FlatStyle.Flat;
                buttonSave.Padding = new Padding(0, 0, 2, 2);

                numericUpDownDelayTime.ForeColor = white;
                numericUpDownDelayTime.BackColor = mediumGrey;

                comboBoxSubtitleFps.ForeColor = white;
                comboBoxSubtitleFps.BackColor = mediumGrey;
                comboBoxSubtitleFps.FlatStyle = FlatStyle.Popup;

                buttonConvert.ForeColor = white;
                buttonConvert.BackColor = mediumGrey;
                buttonConvert.FlatStyle = FlatStyle.Flat;
            }
        }
    }
    enum DarkThemeMode { System, Always, Never }
}
